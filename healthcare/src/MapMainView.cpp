/*
 * MainView.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: shejan
 */

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#include "MapMainView.h"
#include "CommonUtil.h"


#define UA_TIZEN_WEB \
		"Mozilla/5.0 (Linux; Tizen 2.1; SAMSUNG GT-I8800) AppleWebKit/537.3 (KHTML, like Gecko) Version/2.1 Mobile Safari/537.3"
#define DEFAULT_LAT		37.253665
#define DEFAULT_LON		127.054968

MapMainView::MapMainView() {
}
MapMainView::~MapMainView() {

}

// if 1 == under, 2 == normal , 3 == over wight, 4 == extremely obese

Evas_Object* MapMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe) {
	init();

	naviframe = mainNaviframe;
	layout = elm_layout_add(parent);

	loadMap();

	Elm_Object_Item *navi_item = elm_naviframe_item_push(parent, "Nearby Hospital", NULL, NULL, layout, NULL);
//	elm_naviframe_item_pop_cb_set(navi_item, __map_view_delete_request_cb, (void *)NULL);

	m_parent_evas_obj = parent;
	m_map_view_layout = layout;
	eext_object_event_callback_add(layout, EEXT_CALLBACK_BACK, layout_back_cb, this);
	return layout;
}

void MapMainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	MapMainView *self = (MapMainView*) data;
	elm_naviframe_item_pop(self->naviframe);
}

void MapMainView::init()
{
	__view_type = MAPS_VIEW_MODE_MY_LOCATION;

	int i = 0;
	for (i = 0; i < 50; i++)
		__m_poi_overlay[i] = NULL;

	__m_poi_overlay_count = 0;

	__m_poi_current_overlay = NULL;

	for (i = 0; i < 1000; i++) {
		__m_maneuver_overlay[i] = NULL;
		__m_route_overlay[i] = NULL;
	}
	__m_maneuver_overlay_count = 0;
	__m_route_overlay_count = 0;

	m_map_obj_layout = NULL;
	m_map_evas_object = NULL;
	m_searchbar_obj = NULL;

	__m_start_overlay = NULL;
	__m_dest_overlay = NULL;

	__is_long_pressed = false;
}


void MapMainView::loadMap()
{
	char edj_path[PATH_MAX] = {0, };

	app_get_resource(MAP_VIEW_EDJ_FILE, edj_path, (int)PATH_MAX);

	dlog_print(DLOG_ERROR, LOG_TAG, "edj_path : %s", edj_path);

	elm_layout_file_set(layout, edj_path, "map-view");
	evas_object_size_hint_weight_set(layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(layout, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(layout);

	if (!m_map_obj_layout) {
		m_map_obj_layout = createMapObject();
		if (m_map_obj_layout == NULL) {
			dlog_print(DLOG_ERROR, LOG_TAG, "failed to create map object");
			return ;
		}
	}

	elm_object_part_content_set(layout, "map", m_map_obj_layout);

//	if (!m_searchbar_obj)
//		m_searchbar_obj = __create_map_searchbar(view_layout);
//
//	if (m_searchbar_obj) {
//		elm_genlist_realized_items_update(m_searchbar_obj);
//		elm_object_part_content_set(view_layout, "searchbar", m_searchbar_obj);
//	}
//
	elm_object_focus_set(m_map_obj_layout, EINA_TRUE);

	elm_object_focus_custom_chain_append(layout, m_map_obj_layout, NULL);
	elm_object_focus_custom_chain_append(layout, m_searchbar_obj, NULL);
}

Evas_Object* MapMainView::createMapObject()
{
	char edj_path[PATH_MAX] = {0, };

	if (m_map_evas_object != NULL) {
		/* WERROR("m_map_evas_object is created already"); */
		return m_map_evas_object;
	}

	m_map_evas_object = elm_map_add(layout);
	if (m_map_evas_object == NULL) {
		dlog_print(DLOG_ERROR, LOG_TAG, "m_map_evas_object is NULL");
		return NULL;
	}

	elm_map_zoom_min_set(m_map_evas_object, 2);
	elm_map_user_agent_set(m_map_evas_object, UA_TIZEN_WEB);
	evas_object_show(m_map_evas_object);

	Evas_Object *map_obj_layout = elm_layout_add(layout);

	app_get_resource(MAP_VIEW_EDJ_FILE, edj_path, (int)PATH_MAX);
	elm_layout_file_set(map_obj_layout, edj_path, "map_object");
	evas_object_size_hint_weight_set(map_obj_layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(map_obj_layout, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(map_obj_layout);
	elm_layout_content_set(map_obj_layout, "content", m_map_evas_object);

//	evas_object_smart_callback_add(m_map_evas_object, "scroll", __maps_scroll_cb, (void *)NULL);
//	evas_object_smart_callback_add(m_map_evas_object, "longpressed", __maps_longpress_cb, (void *)NULL);
//	evas_object_smart_callback_add(m_map_evas_object, "clicked", __maps_clicked_cb, (void *)NULL);

	elm_map_zoom_set(m_map_evas_object, 15);
	elm_map_region_bring_in(m_map_evas_object, DEFAULT_LON, DEFAULT_LAT);

	return map_obj_layout;
}
