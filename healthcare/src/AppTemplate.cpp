/*
 * AppTemplate.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: nafeez
 */
#include "AppTemplate.h"
#include "MainView.h"

AppTemplate::AppTemplate() {

}
AppTemplate::~AppTemplate(){
}
void AppTemplate::appCreated() {
}

void AppTemplate::appResumed() {
}
void AppTemplate::appTerminated() {
}
void AppTemplate::appPaused() {
}
void AppTemplate::appControlCalled() {
}


void * getAppTemplate() {
	AppTemplate *app = new MainView();
	return ((void *)app);
}

void appCreate(void* data) {
	AppTemplate *app = (AppTemplate*) data;
	app->appCreated();
}
void appTerminate(void* data){
	AppTemplate *app = (AppTemplate*) data;
	app->appTerminated();
}
void appPause(void *data) {
	AppTemplate *app = (AppTemplate*) data;
	app->appPaused();
}
void appResume(void *data) {
	AppTemplate *app = (AppTemplate*) data;
	app->appResumed();
}
void appControl(void*data) {
	AppTemplate *app = (AppTemplate*) data;
	app->appControlCalled();
}




