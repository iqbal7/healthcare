#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <string>
using namespace std;
#include "AmbulanceMainView.h"
#include "CommonUtil.h"

typedef struct item_data
{
	int index;
	Elm_Object_Item *item;
	Eina_Bool expanded;
} item_data_s;

class Ambulance {
public:
	int index;
	string ambulanceName;
	string telephone;
};

Ambulance ambulances[] = { { 0, "Ambulance A", "0184294884" }, { 1, "Ambulance B",
		"18839138" }, {2, "Ambulance C", "9983768"} };

AmbulanceMainView::AmbulanceMainView(Evas_Object *_naviframe) {
	naviframe = _naviframe;
}

void AmbulanceMainView::layout_back_cb(void *data, Evas_Object *obj, void *event_info) {
	AmbulanceMainView *self = (AmbulanceMainView*) data;
	elm_naviframe_item_pop(self->naviframe);
}

char* AmbulanceMainView::_text_get_cb(void *data, Evas_Object *obj, const char *part)
{
	Ambulance* ambulance = static_cast<Ambulance*>(data);
	if (strcmp(part, "elm.text") == 0) {
				return strdup(ambulance->ambulanceName.c_str());
			} else {
				return NULL;
			}
}

/*
char* AmbulanceMainView::_text_get2_cb(void *data, Evas_Object *obj, const char *part)
{
	//item_data_s *id = data;
	// Check this is text for the expected part
	   if (strcmp(part, "elm.text") == 0)
	   {
	      return strdup("AMBULANCE B");
	   }

	   else
	   {
	      return NULL;
	   }


}


Evas_Object* AmbulanceMainView::_item_content_get(void *data, Evas_Object *obj, const char *part)
{
	int i = (int) (uintptr_t) data;

	   if (strcmp(part, "contacts_ic_circle_btn_call.png") == 0) {
	      Evas_Object *bg = elm_bg_add(obj);
	      elm_bg_color_set(bg, 255 * cos(i / (double) 10), 0, i % 255);
	      return bg;
	   }
	   else if (strcmp(part, "elm.swallow.end") == 0) {
	      Evas_Object *bg = elm_bg_add(obj);
	      elm_bg_color_set(bg, 0, 255 * sin(i / (double) 10), i % 255);
	      return bg;
	   }
	   else {
	      return NULL;
	   }
}
*/
void AmbulanceMainView::gl_del_cb(void *data, Evas_Object *obj)
{
	/* Unrealized callback can be called after this. */
	/* Accessing item_data_s can be dangerous on unrealized callback. */
	//item_data_s *id = data;
	//free(id);
}

void AmbulanceMainView::call_number(char* number) {
	app_control_h app_control = NULL;
	app_control_create(&app_control);
	app_control_set_app_id(app_control, "com.samsung.call");
	app_control_add_extra_data(app_control, "number", number);
	app_control_add_extra_data(app_control, "launch-type", "MO");
	if(app_control_send_launch_request(app_control, NULL, NULL) == APP_CONTROL_ERROR_NONE)
	   //LOGI("Phone launched!");

	app_control_destroy(app_control);
}

void AmbulanceMainView::AButtonClicked(void *data, Evas_Object *obj,
		void *event_info) {
	Ambulance* ambulance = static_cast<Ambulance*>(data);
	dlog_print(DLOG_DEBUG, "EmerAmbulance", "%s is called", ambulance->ambulanceName.c_str());

	dlog_print(DLOG_DEBUG, "EmerAmbulance", "called %s", ambulance->ambulanceName.c_str());

	char* chr = strdup(ambulance->telephone.c_str());
	call_number(chr);
}

void AmbulanceMainView::genlist_cb(void *data, Evas_Object *obj, void *event_info)
{
	AmbulanceMainView* self = static_cast<AmbulanceMainView*> (data);

	Evas_Object *genlist;
	Elm_Object_Item *it = (Elm_Object_Item*) event_info;
	Elm_Genlist_Item_Class *itc = NULL;



	const char *style = elm_object_item_text_get(it);

	printf("Item style: %s\n", style);
	elm_list_item_selected_set(it, EINA_FALSE);

	/* Create item class

		itc = elm_genlist_item_class_new();
		itc->item_style = "default";
		itc->func.content_get = NULL;
		itc->func.text_get = _text_get_cb;
		itc->func.del = gl_del_cb;

		itc2 = elm_genlist_item_class_new();
		itc2->item_style = "default";
		itc2->func.content_get = NULL;
		itc2->func.text_get = _text_get2_cb;
		itc2->func.del = gl_del_cb;

	genlist = elm_genlist_add(self->naviframe);


	//elm_genlist_block_count_set(genlist, 3);*/

	genlist = elm_genlist_add(self->naviframe);

	elm_genlist_mode_set(genlist, ELM_LIST_EXPAND);
	printf("Compress mode enabled\n");

	for (int i = 0; i < 3; i++) {
	itc = elm_genlist_item_class_new();
			itc->item_style = "default";
			itc->func.content_get = NULL;
			itc->func.text_get = _text_get_cb;
			itc->func.del = gl_del_cb;
			elm_genlist_item_append(genlist, // genlist object
											itc,  // item class (append group_index item per 7 items)
											(void*) &ambulances[i],   // item class user data
											NULL, // parent
											ELM_GENLIST_ITEM_NONE, // item type
											AButtonClicked, // select smart callback
											(void*) &ambulances[i]);  // smart callback user data
			elm_genlist_item_class_free(itc);
	}/*
	if (strcmp("Ambulance Service", style))
		elm_genlist_homogeneous_set(genlist, EINA_TRUE);

	item_data_s *id= (item_data_s*)calloc(sizeof(item_data_s), 1);

		elm_genlist_item_append(genlist, // genlist object
								itc,  // item class (append group_index item per 7 items)
								NULL,   // item class user data
								NULL, // parent
								ELM_GENLIST_ITEM_NONE, // item type
								NULL, // select smart callback
								id);  // smart callback user data

		elm_genlist_item_append(genlist, // genlist object
							itc2,  // item class
							NULL,   // item class user data
							NULL,
							ELM_GENLIST_ITEM_TREE, // item type
							NULL, // select smart callback
							id);  // smart callback user data


	elm_genlist_item_class_free(itc);
	if (itc2) elm_genlist_item_class_free(itc2);*/



	elm_naviframe_item_push(self->naviframe, style, NULL, NULL, genlist, NULL);

}

Evas_Object* AmbulanceMainView::create(Evas_Object* parent, Evas_Object* mainNaviframe)
{
	Evas_Object *list;
	naviframe = mainNaviframe;
	/* List */
	list = elm_list_add(parent);
	elm_list_mode_set(list, ELM_LIST_COMPRESS);
//	evas_object_smart_callback_add(list, "selected", list_selected_cb, NULL);

	/* Main Menu Items Here*/

//	elm_list_item_append(list, "Doctor", NULL, NULL, calldoc, nf);
//	elm_list_item_append(list, "Ambulance", NULL, NULL, ambulance, nf);
	//elm_list_item_append(list, "Call 2 Doc", NULL, NULL, genlist_test_cb, this);
	elm_list_item_append(list, "Ambulance Service", NULL, NULL, genlist_cb, this);

	elm_list_go(list);

	eext_object_event_callback_add(list, EEXT_CALLBACK_BACK, layout_back_cb,
						this);


	return list;
}
