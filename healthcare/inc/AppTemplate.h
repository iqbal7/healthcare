/*
 * AppTemplate.h
 *
 *  Created on: Jun 1, 2016
 *      Author: nafeez
 */

#ifndef APPTEMPLATE_H_
#define APPTEMPLATE_H_

#ifdef __cplusplus
class AppTemplate {
public:
	AppTemplate();
	virtual ~AppTemplate();

public:
	virtual void appCreated();
	virtual void appResumed();
	virtual void appTerminated();
	virtual void appPaused();
	virtual void appControlCalled();
};
#endif
#ifdef __cplusplus
extern "C" {
#endif
	void * getAppTemplate();
	void appCreate(void* data);
	void appTerminate(void* data);
	void appPause(void *data);
	void appResume(void *data);
	void appControl(void*data);
#ifdef __cplusplus
}
#endif



#endif /* APPTEMPLATE_H_ */
